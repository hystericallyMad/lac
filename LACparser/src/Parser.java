import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: jmurphy
 * Date: 1/24/13
 * Time: 11:59 AM
 * To change this template use File | Settings | File Templates.
 */
public class Parser {
    public Parser(){
    }

    public static void main(String[] args) {
        String[] filePath;
        filePath = OpenDialog();

        String filePathOut = filePath[0] + "\\txtToXMLFiles\\" + filePath[1].substring(0,filePath[1].indexOf(".txt")) + "OUT.xml";
        String filePathIn = filePath[0] + "\\" + filePath[1];
        System.out.println("Out: " + filePathOut);
        System.out.println("In:  " + filePathIn);
        System.out.println("\u00A7\n");
        new DomWriter(filePathIn, filePathOut).writeXMLFile();
        return;
    }
    public static String[] OpenDialog() {
        String[] filePath = new String[2];

        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File("C:\\Users\\jmurphy\\Desktop\\Projects\\cfr\\State Codes\\2012"));
        FileNameExtensionFilter ft0 = new FileNameExtensionFilter("TEXT Files", "txt");

        chooser.addChoosableFileFilter(ft0);

        int r = chooser.showOpenDialog(new JFrame());
        if (r == JFileChooser.APPROVE_OPTION) {
            filePath[0] = chooser.getCurrentDirectory().getPath();
            filePath[1] = chooser.getSelectedFile().getName();
        }
        return filePath;
    }
}
