
import org.w3c.dom.*;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.sql.*;
import java.nio.charset.Charset;

/**
 * Created with IntelliJ IDEA.
 * User: jmurphy
 * Date: 1/24/13
 * Time: 12:18 PM
 * To change this template use File | Settings | File Templates.
 */



public class DomWriter {
    private PrintWriter out;
    private BufferedReader reader;

    public DomWriter (String inputFile, String xmlFile){
        try {
            out = new PrintWriter(new FileOutputStream(xmlFile));
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile), "UTF-8") );
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void writeXMLFile(){
        Document outDocument = null;
        try {
            outDocument = buildNewDOM();
            createXMLFile(outDocument);
            loadDBTables(outDocument);
        }catch (Exception e){
            e.printStackTrace(System.err);
        }
    }
    public Document buildNewDOM()  {
        Document outDocument = null;
        String currentLine = null;
        String currentLineSection = null;
        Element currentElement = null, currentTitle = null, currentPart = null, currentSubPart = null,
                currentChapter = null, currentSubChapter = null, currentSection = null,
                parentPart = null, parentChapter = null;
        int currentIndex;
        String sectionSymbol = "\u00A7", title = "Title", part = "Part",
                subpart = "Subpart", chapter = "Chapter",subchapter = "Subchapter";
        String tab = "\t";
        String partParent = "", subpartParent = "", chapterParent = "", subchapterParent = "", sectionParent ="";
        try {
            DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dFactory.newDocumentBuilder();
            outDocument =  builder.newDocument();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        try {
            reader.readLine();
            while((currentLine = reader.readLine()) != null){
                currentIndex = currentLine.indexOf(tab);
                currentLineSection = currentLine.substring(0, currentIndex - 1);
                if (currentLineSection.startsWith(title)){
                    System.out.println("Found title: " + currentLineSection);
                    currentTitle = outDocument.createElement("TITLE");
                    currentElement = currentTitle;
                    outDocument.appendChild(currentTitle);
                    Element number = outDocument.createElement("NUMBER");
                    currentTitle.appendChild(number);
                    number.appendChild(outDocument.createTextNode(currentLineSection));
                    Element citation = outDocument.createElement("CITATION");
                    currentTitle.appendChild(citation);
                    citation.appendChild(outDocument.createTextNode("LAC " + currentTitle.getTextContent().substring(currentTitle.getTextContent().indexOf("Title ") + 6)));
                    partParent = citation.getTextContent();
                }else if(currentLineSection.startsWith(part)){
                    System.out.println("Found part: " + currentLineSection);
                    currentPart = outDocument.createElement("PART");
                    currentElement = currentPart;
                    parentPart =currentPart;
                    currentTitle.appendChild(currentPart);
                    Element number = outDocument.createElement("NUMBER");
                    currentPart.appendChild(number);
                    number.appendChild(outDocument.createTextNode(currentLineSection));
                    Element citation = outDocument.createElement("CITATION");
                    currentPart.appendChild(citation);
                    citation.appendChild(outDocument.createTextNode(partParent + ":" + currentLineSection.substring(5)));
                    chapterParent = citation.getTextContent();
                    subchapterParent = chapterParent;
                    subpartParent =chapterParent;
                }else if(currentLineSection.startsWith(subpart)){
                    System.out.println("Found subpart: " + currentLineSection);
                    currentSubPart = outDocument.createElement("SUBPART");
                    currentElement = currentSubPart;
                    parentPart =currentSubPart;
                    currentPart.appendChild(currentSubPart);
                    Element number = outDocument.createElement("NUMBER");
                    currentSubPart.appendChild(number);
                    number.appendChild(outDocument.createTextNode(currentLineSection));
                    Element citation = outDocument.createElement("CITATION");
                    currentSubPart.appendChild(citation);
                    citation.appendChild(outDocument.createTextNode(subpartParent + "." + currentLineSection));
                }else if(currentLineSection.startsWith(chapter)){
                    System.out.println("Found chapter: " + currentLineSection);
                    currentChapter = outDocument.createElement("CHAPTER");
                    currentElement =currentChapter;
                    parentChapter =currentChapter;
                    parentPart.appendChild(currentChapter);
                    Element number = outDocument.createElement("NUMBER");
                    currentChapter.appendChild(number);
                    number.appendChild(outDocument.createTextNode(currentLineSection));
                    Element citation = outDocument.createElement("CITATION");
                    currentChapter.appendChild(citation);
                    citation.appendChild(outDocument.createTextNode(chapterParent + "." + currentLineSection));
                    sectionParent = chapterParent;
                }else if(currentLineSection.startsWith(subchapter)){
                    System.out.println("Found subchapter: " + currentLineSection);
                    currentSubChapter = outDocument.createElement("SUBCHAP");
                    currentElement = currentSubChapter;
                    parentChapter = currentSubChapter;
                    currentChapter.appendChild(currentSubChapter);
                    Element number = outDocument.createElement("NUMBER");
                    currentSubChapter.appendChild(number);
                    number.appendChild(outDocument.createTextNode(currentLineSection));
                    Element citation = outDocument.createElement("CITATION");
                    currentSubChapter.appendChild(citation);
                    citation.appendChild(outDocument.createTextNode(subchapterParent + "." + currentLineSection));
                }else if(currentLineSection.startsWith(sectionSymbol)){
                    System.out.println("Found section: " + currentLineSection);
                    currentSection = outDocument.createElement("SECTION");
                    currentElement = currentSection;
                    parentChapter.appendChild(currentSection);
                    Element number = outDocument.createElement("NUMBER");
                    currentSection.appendChild(number);
                    number.appendChild(outDocument.createTextNode(currentLineSection));
                    Element citation = outDocument.createElement("CITATION");
                    currentSection.appendChild(citation);
                    citation.appendChild(outDocument.createTextNode(sectionParent + "." + currentLineSection));
                }
                currentLineSection = currentLine.substring(currentIndex +1, currentLine.lastIndexOf(tab));
                Element name = outDocument.createElement("NAME");
                currentElement.appendChild(name);
                name.appendChild(outDocument.createTextNode(currentLineSection));
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (StringIndexOutOfBoundsException e){
            System.err.println(e.getMessage() + " " + currentLine + " " + currentLineSection);
            return outDocument;
        }


        return outDocument;
    }

    public void createXMLFile(Document document){
        TransformerFactory tFactory = TransformerFactory.newInstance();
        try {
            Transformer trans = tFactory.newTransformer();
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(document);
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            trans.transform(source, result);
            System.out.println(writer.toString());
            out.println(writer.toString());
            out.close();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (TransformerException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private class NodeInfo{
        int id;
        ParentType thisType;
        public NodeInfo(int id, int type){
            this.id = id;
            switch(type){
                case 1:
                    thisType = ParentType.Part;
                    break;
                case 2:
                    thisType = ParentType.SubPart;
                    break;
                case 3:
                    thisType = ParentType.Chapter;
                    break;
                case 4:
                    thisType = ParentType.SubChapter;
                    break;
            }

        }
        public int getNodeID(){
            return this.id;
        }
        public ParentType getNodeType(){
            return this.thisType;
        }
    }

    public void loadDBTables(Document document){
        NodeList titleNodeList = document.getElementsByTagName("TITLE").item(0).getChildNodes();
        int titleID = -1, sectionID = -1;
        NodeInfo partInfo = null, subPartInfo = null;
        String titleNum, partNum, subPartNum;
        String titleName, partName, subPartName;
        String titleCitation, partCitation, subPartCitation;
        Element titleNumElement = null, titleNameElement = null,titleCitationElement=null, partElement = null;
        int returnedArray[];
        for(int i = 0; i < titleNodeList.getLength(); i++){
            if (titleNodeList.item(i).getNodeName() == "NUMBER"){
                titleNumElement = (Element) titleNodeList.item(i);
            }else if (titleNodeList.item(i).getNodeName() == "NAME"){
                titleNameElement = (Element) titleNodeList.item(i);
            }else if (titleNodeList.item(i).getNodeName()  =="CITATION"){
                titleCitationElement = (Element) titleNodeList.item(i);
            }else if (titleNodeList.item(i).getNodeName() == "PART"){
                partElement = (Element) titleNodeList.item(i);
            }
            if (titleNumElement != null && titleNameElement != null && titleCitationElement != null){
                titleNum = titleNumElement.getTextContent();
                titleName = titleNameElement.getTextContent();
                titleCitation = titleCitationElement.getTextContent();
                titleID = lacTitle(titleNum,  titleName,titleCitation);
                titleNumElement = null;
                titleNameElement = null;
            }
            if (partElement != null){
                NodeList partNodeList = partElement.getChildNodes();
                Element partNumElement = null, partNameElement = null, partCitationElement=null, subpartElement = null, chapterElement = null;
                for (int ii =0; ii < partNodeList.getLength(); ii++){
                    if (partNodeList.item(ii).getNodeName() == "NUMBER"){
                        partNumElement = (Element) partNodeList.item(ii);
                    }else if(partNodeList.item(ii).getNodeName() == "NAME"){
                        partNameElement = (Element) partNodeList.item(ii);
                    }else if(partNodeList.item(ii).getNodeName() == "SUBPART"){
                        subpartElement = (Element) partNodeList.item(ii);
                    }else if(partNodeList.item(ii).getNodeName() == "CITATION"){
                        partCitationElement = (Element) partNodeList.item(ii);
                    }else if(partNodeList.item(ii).getNodeName() == "CHAPTER"){
                        chapterElement = (Element) partNodeList.item(ii);
                    }

                    if (partNumElement != null && partNameElement != null && partCitationElement != null){
                        partNum = partNumElement.getTextContent();
                        partName = partNameElement.getTextContent();
                        partCitation = partCitationElement.getTextContent();
                        returnedArray = lacPart(titleID, partNum, partName, partCitation);
                        partInfo = new NodeInfo(returnedArray[0],returnedArray[1]);
                        partNumElement = null;
                        partNameElement =  null;
                    }
                    if (subpartElement != null){
                        NodeList subpartNodeList = subpartElement.getChildNodes();
                        Element subpartNumElement = null, subpartNameElement = null, subpartCitationElement= null;
                        for (int iii = 0; iii < subpartNodeList.getLength(); iii++){
                            if (subpartNodeList.item(iii).getNodeName() == "NUMBER"){
                                subpartNumElement = (Element) subpartNodeList.item(iii);
                            }else if(subpartNodeList.item(iii).getNodeName() == "NAME"){
                                subpartNameElement = (Element) subpartNodeList.item(iii);
                            }else if(subpartNodeList.item(iii).getNodeName() == "CITATION"){
                                subpartCitationElement = (Element) subpartNodeList.item(iii);
                            }else if(subpartNodeList.item(iii).getNodeName() == "CHAPTER"){
                                chapterElement = (Element)subpartNodeList.item(iii);
                            }
                            if (subpartNumElement != null && subpartNameElement != null && subpartCitationElement != null){
                                subPartNum = subpartNumElement.getTextContent();
                                subPartName = subpartNameElement.getTextContent();
                                subPartCitation = subpartCitationElement.getTextContent();
                                returnedArray = null;
                                returnedArray = lacSubPart(partInfo.getNodeID(),subPartNum, subPartName,subPartCitation);
                                subPartInfo = new NodeInfo(returnedArray[0],returnedArray[1]);
                                subpartNumElement = null;
                                subpartNameElement = null;
                            }
                            if (chapterElement != null){
                                processChapterElement(chapterElement, subPartInfo);
                                chapterElement = null;
                            }
                        }
                        subpartElement = null;
                    }
                    if (chapterElement != null){
                        processChapterElement(chapterElement, partInfo);
                        chapterElement = null;
                    }
                }

                partElement = null;
            }
        }


    }

    public void processChapterElement(Element chapterElement, NodeInfo passedParentInfo){
        NodeList chapterNodeList = chapterElement.getChildNodes();
        Element chapterNumElement = null, chapterNameElement = null, subChapterElement = null, sectionElement = null,
                chapterCitationElement = null;
        String chapterNum, chapterName, chapterCitation, subChapterNum, subChapterName, subChapterCitation;
        NodeInfo chapterInfo = null, subChapterInfo = null;
        int returnArray[] = null;
        for (int i = 0; i < chapterNodeList.getLength(); i++){
            if (chapterNodeList.item(i).getNodeName() == "NUMBER"){
                chapterNumElement = (Element) chapterNodeList.item(i);
            }else if(chapterNodeList.item(i).getNodeName() == "NAME"){
                chapterNameElement = (Element) chapterNodeList.item(i);
            }else if(chapterNodeList.item(i).getNodeName() == "CITATION"){
                chapterCitationElement = (Element) chapterNodeList.item(i);
            }else if(chapterNodeList.item(i).getNodeName() == "SUBCHAP"){
                subChapterElement = (Element) chapterNodeList.item(i);
            }else if(chapterNodeList.item(i).getNodeName() == "SECTION"){
                sectionElement = (Element) chapterNodeList.item(i);
            }
            if (chapterNumElement != null && chapterNameElement != null && chapterCitationElement != null){
                chapterNum = chapterNumElement.getTextContent();
                chapterName = chapterNameElement.getTextContent();
                chapterCitation = chapterCitationElement.getTextContent();
                returnArray = lacChapter(passedParentInfo.getNodeID(),passedParentInfo.getNodeType(),chapterNum,
                        chapterName, chapterCitation );
                chapterInfo = new NodeInfo(returnArray[0],returnArray[1]);
                chapterNumElement = null;
                chapterNameElement = null;
            }
            if (subChapterElement != null){
                NodeList subChapterNodeList = subChapterElement.getChildNodes();
                Element subChapterNumElement = null, subChapterNameElement = null, subChapterCitationElement = null;
                for (int ii = 0; ii < subChapterNodeList.getLength(); ii++){
                    if (subChapterNodeList.item(ii).getNodeName() == "NUMBER"){
                        subChapterNumElement = (Element) subChapterNodeList.item(ii);
                    }else if(subChapterNodeList.item(ii).getNodeName() == "NAME"){
                        subChapterNameElement = (Element) subChapterNodeList.item(ii);
                    }else if(subChapterNodeList.item(ii).getNodeName() == "CITATION"){
                        subChapterCitationElement = (Element) subChapterNodeList.item(ii);
                    }else if(subChapterNodeList.item(ii).getNodeName() == "SECTION"){
                        sectionElement = (Element) subChapterNodeList.item(ii);
                    }
                    if (subChapterNumElement != null && subChapterNameElement != null){
                        subChapterNum = subChapterNumElement.getTextContent();
                        subChapterName = subChapterNameElement.getTextContent();
                        subChapterCitation = subChapterCitationElement.getTextContent();
                        returnArray = null;
                        returnArray = lacSubChapter(chapterInfo.getNodeID(),subChapterNum,subChapterName,subChapterCitation);
                        subChapterInfo = new NodeInfo(returnArray[0],returnArray[1]);
                        subChapterNumElement = null;
                        subChapterNameElement = null;
                    }
                    if (sectionElement != null){
                        processSectionElement(sectionElement, subChapterInfo);
                    }
                }
            }
            if (sectionElement != null){
                processSectionElement(sectionElement, chapterInfo);
                sectionElement = null;
            }
        }

    }

    public void processSectionElement(Element sectionElement, NodeInfo passedParentInfo){
        NodeList sectionNodeList = sectionElement.getChildNodes();
        Element sectionNumElement = null, sectionNameElement = null, sectionCitationElement = null;
        String sectionNum, sectionName, sectionCitation;
        NodeInfo sectionInfo;
        for (int i = 0; i < sectionNodeList.getLength(); i++){
            if (sectionNodeList.item(i).getNodeName() == "NUMBER"){
                sectionNumElement = (Element) sectionNodeList.item(i);
            }else if(sectionNodeList.item(i).getNodeName() == "NAME"){
                sectionNameElement = (Element) sectionNodeList.item(i);
            }else if(sectionNodeList.item(i).getNodeName() == "CITATION"){
                sectionCitationElement = (Element) sectionNodeList.item(i);
            }
            if (sectionNumElement != null && sectionNameElement != null){
                sectionNum = sectionNumElement.getTextContent();
                sectionName = sectionNameElement.getTextContent();
                sectionCitation = sectionCitationElement.getTextContent();
                int sectionID = lacSection(passedParentInfo.getNodeID(),passedParentInfo.getNodeType(),sectionNum, sectionName,sectionCitation);
                sectionNumElement = null;
                sectionNameElement = null;
            }
        }
    }

    public enum  ParentType{
        Part (1), SubPart (2), Chapter (3), SubChapter (4);

        private int value;

        private ParentType(int value){
            this.value = value;
        }
        public void setParentType(int value){
            this.value = value;
        }
        public int getParentType(){
            return value;
        }
    };

    public int lacTitle(String titleNum, String titleName,String titleCitation){
        PreparedStatement cs = null;
        Connection conn = null;
        ResultSet rs = null;
        boolean exist = false;
        int titleID = -1;

        try{
            conn = getConnection();
            cs = conn.prepareStatement("SELECT titleID FROM LAC.Title WHERE titleNum = ?");
            cs.setString(1,titleNum);
            rs = cs.executeQuery();

            while(rs.next()){
                exist =true;
                titleID = rs.getInt("titleID");
            }

            if (exist){
                System.out.println(titleID);
                cs = conn.prepareStatement("UPDATE LAC.Title SET titleNum = ?, titleName = ?, titleCitation = ? WHERE titleID = ?");
                cs.setString(1,titleNum);
                cs.setString(2,titleName);
                cs.setString(3,titleCitation);
                cs.setInt(4,titleID);
                int i = cs.executeUpdate();
                if (i !=1){
                    System.out.println("Error updating on LAC.Title");
                    Throwable SQLERRUPDATE = new Exception("ERROR UPDATING ON LAC.TITLE TABLE");
                    throw SQLERRUPDATE;
                }
            }else{
                cs = conn.prepareStatement("INSERT LAC.Title VALUES (?,?,?)");
                cs.setString(1,titleNum);
                cs.setString(2,titleName);
                cs.setString(3,titleCitation);
                int i = cs.executeUpdate();
                if (i != 1){
                    System.out.println("Error inserting on LAC.Title");
                    Throwable SQLERRINSERT = new Exception("ERROR INSERTING ON LAC.TITLE TABLE");
                    throw SQLERRINSERT;
                }
                cs.clearParameters();
                cs = conn.prepareStatement("SELECT TOP 1 titleID FROM LAC.Title ORDER BY titleID DESC");
                rs = cs.executeQuery();
                while(rs.next()){
                    titleID = rs.getInt("titleID");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Throwable throwable) {
            throwable.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            try {
                rs.close();
                cs.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        return titleID;
    }

    public int[] lacPart(int titleID, String partNum, String partName, String partCitation){
        PreparedStatement cs = null;
        Connection conn = null;
        ResultSet rs = null;
        boolean exist = false;
        int partID = -1;
        ParentType thisType = ParentType.Part;
        int values[] = new int[2];
        values[1] = thisType.getParentType();
        try {
            conn = getConnection();
            cs = conn.prepareStatement("SELECT partID FROM LAC.Part WHERE titleID = ? AND partNum = ?");
            cs.setInt(1,titleID);
            cs.setString(2,partNum);
            rs = cs.executeQuery();

            while(rs.next()){
                exist = true;
                partID = rs.getInt("partID");
            }

            if (exist){
                cs.clearParameters();
                cs = conn.prepareStatement("UPDATE LAC.Part SET titleID = ?, partNum = ?, partName = ?, partCitation = ? " +
                        "WHERE partID = ?");
                cs.setInt(1, titleID);
                cs.setString(2, partNum);
                cs.setString(3, partName);
                cs.setString(4,partCitation);
                cs.setInt(5,partID);
                int i = cs.executeUpdate();
                if (i != 1){
                    Throwable SQLERRUPDATE = new Exception("ERROR UPDATING ON LAC.PART TABLE");
                    throw SQLERRUPDATE;
                }
            }else{
                cs.clearParameters();
                cs = conn.prepareStatement("INSERT LAC.Part VALUES (?,?,?,?)");
                cs.setInt(1,titleID);
                cs.setString(2,partNum);
                cs.setString(3,partName);
                cs.setString(4,partCitation);
                int i = cs.executeUpdate();
                if (i != 1){
                    Throwable SQLERRINSERT = new Exception("ERROR INSERTING ON LAC.PART TABLE");
                    throw SQLERRINSERT;
                }
                cs.clearParameters();
                cs = conn.prepareStatement("SELECT TOP 1 partID FROM LAC.Part ORDER BY partID DESC");
                rs = cs.executeQuery();
                while(rs.next()){
                    partID = rs.getInt("partID");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Throwable throwable) {
            throwable.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }finally {
            try {
                rs.close();
                cs.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        values[0] = partID;
        return values;
    }

    public int[] lacSubPart(int partID, String subPartNum, String subPartName, String subPartCitation){
        PreparedStatement cs = null;
        Connection conn = null;
        ResultSet rs = null;
        boolean exist = false;
        int subPartID = -1;
        ParentType thisType = ParentType.SubPart;
        int values[] = new int[2];
        values[1] = thisType.getParentType();
        try {
            conn = getConnection();
            cs = conn.prepareStatement("SELECT subPartID FROM LAC.subPart WHERE partID = ? AND subPartNum = ?");
            cs.setInt(1,partID);
            cs.setString(2,subPartNum);
            rs = cs.executeQuery();

            while(rs.next()){
                exist = true;
                subPartID = rs.getInt("subPartID");
            }

            if (exist){
                cs.clearParameters();
                cs = conn.prepareStatement("UPDATE LAC.subPart SET partID = ?, subPartNum = ?, subPartName = ?, " +
                        "subPartCitation = ? WHERE subPartID = ?");
                cs.setInt(1, partID);
                cs.setString(2, subPartNum);
                cs.setString(3, subPartName);
                cs.setString(4, subPartCitation);
                cs.setInt(5,subPartID);
                int i = cs.executeUpdate();
                if (i != 1){
                    Throwable SQLERRUPDATE = new Exception("ERROR UPDATING ON LAC.subPart TABLE");
                    throw SQLERRUPDATE;
                }
            }else{
                cs.clearParameters();
                cs = conn.prepareStatement("INSERT LAC.subPart VALUES (?,?,?,?)");
                cs.setInt(1,partID);
                cs.setString(2,subPartNum);
                cs.setString(3,subPartName);
                cs.setString(4,subPartCitation);
                int i = cs.executeUpdate();
                if (i != 1){
                    Throwable SQLERRINSERT = new Exception("ERROR INSERTING ON LAC.subPart TABLE");
                    throw SQLERRINSERT;
                }
                cs.clearParameters();
                cs = conn.prepareStatement("SELECT TOP 1 subPartID FROM LAC.subPart ORDER BY subPartID DESC");
                rs = cs.executeQuery();
                while(rs.next()){
                    subPartID = rs.getInt("subPartID");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Throwable throwable) {
            throwable.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }finally {
            try {
                rs.close();
                cs.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        values[0] = subPartID;
        return values;
    }

    public int[] lacChapter(int parentID, ParentType parentType, String chapterNum, String chapterName, String chapterCitation){
        PreparedStatement cs = null;
        Connection conn = null;
        ResultSet rs = null;
        boolean exist = false;
        int chapterID = -1;
        ParentType thisType = ParentType.Chapter;
        int values[] = new int[2];
        values[1] = thisType.getParentType();
        try {
            conn = getConnection();
            cs = conn.prepareStatement("SELECT chapterID FROM LAC.Chapter WHERE parentID = ? AND parentType = ? AND chapterNum = ?");
            cs.setInt(1, parentID);
            cs.setInt(2, parentType.getParentType());
            cs.setString(3, chapterNum);
            rs = cs.executeQuery();

            while(rs.next()){
                exist = true;
                chapterID = rs.getInt("chapterID");
            }

            if (exist){
                cs.clearParameters();
                cs = conn.prepareStatement("UPDATE LAC.Chapter SET parentID = ?, parentType = ?, chapterNum = ?, chapterName = ?, " +
                        "chapterCitation = ? WHERE chapterID = ?");
                cs.setInt(1,parentID);
                cs.setInt(2,parentType.getParentType());
                cs.setString(3,chapterNum);
                cs.setString(4,chapterName);
                cs.setString(5,chapterCitation);
                cs.setInt(6,chapterID);
                int i = cs.executeUpdate();
                if (i != 1){
                    Throwable SQLERRUPDATE = new Exception("ERROR UPDATING ON LAC.CHAPTER TABLE");
                    throw SQLERRUPDATE;
                }
            }else{
                cs.clearParameters();
                cs = conn.prepareStatement("INSERT LAC.Chapter VALUES (?,?,?,?,?)");
                cs.setInt(1, parentID);
                cs.setInt(2, parentType.getParentType());
                cs.setString(3, chapterNum);
                cs.setString(4, chapterName);
                cs.setString(5, chapterCitation);
                int i = cs.executeUpdate();
                if (i != 1){
                    Throwable SQLERRINSERT = new Exception("ERROR INSERTING ON LAC.CHAPTER TABLE");
                    throw SQLERRINSERT;
                }
                cs.clearParameters();
                cs = conn.prepareStatement("SELECT TOP 1 chapterID FROM LAC.Chapter ORDER BY chapterID DESC");
                rs = cs.executeQuery();
                while(rs.next()){
                    chapterID = rs.getInt("chapterID");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Throwable throwable) {
            throwable.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }finally {
                try {
                    rs.close();
                    cs.close();
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
        }
        values[0] = chapterID;
        return values;
    }

    public int[] lacSubChapter(int chapterID, String subChapterNum, String subChapterName, String subChapterCitation){
        PreparedStatement cs = null;
        Connection conn = null;
        ResultSet rs = null;
        boolean exist = false;
        int subChapterID = -1;
        ParentType thisType = ParentType.SubChapter;
        int values[] = new int[2];
        values[1] = thisType.getParentType();

        try {
            conn = getConnection();
            cs = conn.prepareStatement("SELECT subChapterID FROM LAC.subChapter WHERE chapterID = ? AND subChapterNum = ?");
            cs.setInt(1,chapterID);
            cs.setString(2,subChapterNum);
            rs = cs.executeQuery();

            while(rs.next()){
                exist = true;
                subChapterID = rs.getInt("subChapterID");
            }

            if (exist){
                cs.clearParameters();
                cs = conn.prepareStatement("UPDATE LAC.subChapter SET chapterID = ?, subChapterNum = ?, subChapterName = ?, " +
                        "subChapterCitation = ? WHERE subChapterID = ?");
                cs.setInt(1, chapterID);
                cs.setString(2, subChapterNum);
                cs.setString(3, subChapterName);
                cs.setString(4, subChapterCitation);
                cs.setInt(5,subChapterID);
                int i = cs.executeUpdate();
                if (i != 1){
                    Throwable SQLERRUPDATE = new Exception("ERROR UPDATING ON LAC.subChapter TABLE");
                    throw SQLERRUPDATE;
                }
            }else{
                cs.clearParameters();
                cs = conn.prepareStatement("INSERT LAC.subChapter VALUES (?,?,?,?)");
                cs.setInt(1,chapterID);
                cs.setString(2,subChapterNum);
                cs.setString(3,subChapterName);
                cs.setString(4,subChapterCitation);
                int i = cs.executeUpdate();
                if (i != 1){
                    Throwable SQLERRINSERT = new Exception("ERROR INSERTING ON LAC.subChapter TABLE");
                    throw SQLERRINSERT;
                }
                cs.clearParameters();
                cs = conn.prepareStatement("SELECT TOP 1 subChapterID FROM LAC.subChapter ORDER BY subChapterID DESC");
                rs = cs.executeQuery();
                while(rs.next()){
                    subChapterID = rs.getInt("subChapterID");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Throwable throwable) {
            throwable.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }finally {
            try {
                rs.close();
                cs.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        values[0] = subChapterID;
        return values;
    }

    public int lacSection(int parentID, ParentType parentType, String sectionNum, String sectionName, String sectionCitation){
        PreparedStatement cs = null;
        Connection conn = null;
        ResultSet rs = null;
        boolean exist = false;
        int sectionID = -1;

        try {
            conn = getConnection();
            cs = conn.prepareStatement("SELECT sectionID FROM LAC.Section WHERE parentID = ? AND parentType = ? " +
                    "AND sectionNum = ?");
            cs.setInt(1, parentID);
            cs.setInt(2, parentType.getParentType());
            cs.setString(3,sectionNum);
            rs = cs.executeQuery();

            while(rs.next()){
                exist = true;
                sectionID = rs.getInt("sectionID");
            }

            if (exist){
                cs.clearParameters();
                cs = conn.prepareStatement("UPDATE LAC.Section SET parentID = ?, parentType = ?, sectionNum = ?, " +
                        "sectionName = ?, sectionCitation = ? WHERE sectionID = ?");
                cs.setInt(1, parentID);
                cs.setInt(2, parentType.getParentType());
                cs.setString(3, sectionNum);
                cs.setString(4, sectionName);
                cs.setString(5, sectionCitation);
                cs.setInt(6, sectionID);
                int i = cs.executeUpdate();
                if (i != 1){
                    Throwable SQLERRUPDATE = new Exception("ERROR UPDATING ON LAC.SECTION TABLE");
                    throw SQLERRUPDATE;
                }
            }else{
                cs.clearParameters();
                cs = conn.prepareStatement("INSERT LAC.Section VALUES (?,?,?,?,?)");
                cs.setInt(1, parentID);
                cs.setInt(2, parentType.getParentType());
                cs.setString(3, sectionNum);
                cs.setString(4, sectionName);
                cs.setString(5,sectionCitation);
                int i = cs.executeUpdate();
                if (i != 1){
                    Throwable SQLERRINSERT = new Exception("ERROR INSERTING ON LAC.SECTION TABLE");
                    throw SQLERRINSERT;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Throwable throwable) {
            throwable.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            try {
                rs.close();
                cs.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        return sectionID;
    }

    public Connection getConnection() throws SQLException {
        Connection conn = null;

        String cs = "jdbc:sqlserver://localhost;databaseName=October_12_2012;";
        String user = "siteSQLAccess";
        String password = "access";
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(cs, user, password);
            System.out.println("connected to " + cs);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        return conn;
    }
}

